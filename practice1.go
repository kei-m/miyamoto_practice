package main

import (
    "fmt"
    "strings"
    "flag"
)

func main(){

    //文字列は-sで指定できるようにする
    var str string
    flag.StringVar(&str, "s", "", "分割したい英文")
    flag.Parse()

    //ピリオドの扱いについて
    //ピリオドを考慮しないならこっち
    str = strings.Replace(str, ".", "", -1)

    //ピリオドを1単語として表示するならこっち
    //str = strings.Replace(str, ".", " .", -1)

    strList := strings.Split(str, " ")
    fmt.Println(strList)
    
    for _, v := range strList{
    fmt.Println(v)
    }
}

/*実行結果(-s "I am Kei. I love bebe-chan.")

[I am Kei I love bebe-chan]
I
am
Kei
I
love
bebe-chan

*/